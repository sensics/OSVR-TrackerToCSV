/** @file
    @brief Implementation

    @date 2014

    @author
    Sensics, Inc.
    <http://sensics.com/osvr>
*/

// Copyright 2014 Sensics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Internal Includes
#include <osvr/ClientKit/Context.h>
#include <osvr/ClientKit/Interface.h>
#include <osvr/Server/RegisterShutdownHandler.h>
#include <osvr/Util/CSV.h>
#include <osvr/Util/CSVCellGroup.h>
#include <osvr/Util/MiniArgsHandling.h>

// Library/third-party includes
// - none

// Standard includes
#include <atomic>
#include <chrono>
#include <fstream>
#include <iostream>
#include <memory>

using namespace osvr::util;

#if 0
std::promise<void> g_shutdownSignal;
void signalShutdown() { g_shutdownSignal.set_value(); }
bool shouldContinue() {
  return std::future_status::ready !=
         g_shutdownSignal.get_future().wait_for(std::chrono::milliseconds(1));
}
#endif
std::atomic_bool g_run{true};
void signalShutdown() { g_run.store(false); }
bool shouldContinue() { return g_run.load(); }

static bool g_reportedTracker = false;

#ifdef USE_INTERVAL
using myClock = std::chrono::high_resolution_clock;
static const auto interval = std::chrono::seconds(1);
static myClock::time_point nextReport;
static bool reportedTracker = false;
static bool outputStarted = false;
std::unique_ptr<osvr::util::StreamCSV> g_csv;
#endif

struct GlobalSettings {
    bool recordVelocity = false;
    bool singleTimeField = false;

    OSVR_TimeValue processTimeStamp(OSVR_TimeValue const *tv) {
        if (!singleTimeField) {
            return *tv;
        }
        if (!gotFirstTime_) {
            firstTime_ = *tv;
            gotFirstTime_ = true;
        }
        // subtract the first time stamp away.
        auto ret = *tv;
        osvrTimeValueDifference(&ret, &firstTime_);
        return ret;
    }

  private:
    bool gotFirstTime_ = false;
    OSVR_TimeValue firstTime_ = {};

} g_settings;

static struct PathAsGroup_t {
} PathAsGroup;

template <typename CSVType> struct TrackerData {
    using type = TrackerData<CSVType>;

    /// constructor for non-grouped (single tracker)
    TrackerData(CSVType &csvObj, osvr::clientkit::ClientContext &ctx,
                std::string const &p)
        : TrackerData(csvObj, ctx, p, false, "") {}

    /// constructor for grouped, numbered trackers.
    TrackerData(CSVType &csvObj, osvr::clientkit::ClientContext &ctx,
                std::string const &p, std::size_t num)
        : TrackerData(csvObj, ctx, p, true, std::to_string(num) + ".") {}

    /// constructor for trackers with group names from their paths
    TrackerData(CSVType &csvObj, osvr::clientkit::ClientContext &ctx,
                std::string const &p, PathAsGroup_t const &)
        : TrackerData(csvObj, ctx, p, true, p + ":") {}
    CSVType *csv;
    std::string path;

    bool group = false;
    std::string groupHeader;
    std::string velGroupHeader;
    std::string angVelDtHeader;
    std::string angVelDeltaGroupHeader;

    osvr::clientkit::Interface iface;

    bool reported = false;
    static void myTrackerCallback(void *userdata,
                                  const OSVR_TimeValue *timestamp,
                                  const OSVR_PoseReport *report) {
        auto *data = static_cast<type *>(userdata);
        if (!data->reported) {
            std::cout << "Got first tracker report from " << data->path
                      << std::endl;
            data->reported = true;
            g_reportedTracker = true;
        }
        using namespace osvr::util;
        auto row = data->csv->row();

        auto tv = g_settings.processTimeStamp(timestamp);
        if (g_settings.singleTimeField) {
            row << cellGroup<DecimalTimeFieldTag>(tv);
        } else {
            row << cellGroup<AbbreviatedTimeMemberFieldsTag>(tv);
        }

        row << cellGroup(data->groupHeader, report->pose.translation)
            << cellGroup(data->groupHeader, report->pose.rotation);
    }

    static void myVelocityCallback(void *userdata,
                                   const OSVR_TimeValue *timestamp,
                                   const OSVR_VelocityReport *report) {
        auto *data = static_cast<type *>(userdata);
        auto row = data->csv->row();
        using namespace osvr::util;

        auto tv = g_settings.processTimeStamp(timestamp);
        if (g_settings.singleTimeField) {
            row << cellGroup<DecimalTimeFieldTag>(tv);
        } else {
            row << cellGroup<AbbreviatedTimeMemberFieldsTag>(tv);
        }

        if (report->state.linearVelocityValid) {
            row << cellGroup(data->velGroupHeader,
                             report->state.linearVelocity);
        }
        if (report->state.angularVelocityValid) {
            row << cell(data->angVelDtHeader, report->state.angularVelocity.dt);
            row << cellGroup(data->angVelDeltaGroupHeader,
                             report->state.angularVelocity.incrementalRotation);
        }
    }

  private:
    /// delegated-to constructor.
    TrackerData(CSVType &csvObj, osvr::clientkit::ClientContext &ctx,
                std::string const &p, bool needGroup,
                std::string const &groupHdr)
        : csv(&csvObj), path(p), group(needGroup), groupHeader(groupHdr),
          velGroupHeader(groupHeader + "vel."),
          angVelDtHeader(groupHeader + "angvel_dt"),
          angVelDeltaGroupHeader(groupHeader + "angvel_delta."),
          iface(ctx.getInterface(path)) {}
};

inline void startOutputCheck(osvr::util::CSV & /*csv*/,
                             bool /*reportedTracker*/,
                             bool & /*outputStarted*/) {
    // no-op for the non-streaming csv file.
}
inline void startOutputCheck(osvr::util::StreamCSV &csv, bool reportedTracker,
                             bool &outputStarted) {
    if (reportedTracker && !outputStarted) {
        csv.startOutput();
        outputStarted = true;
        std::cout << "Saved initial rows to CSV, now in streaming mode..."
                  << std::endl;
    }
}

inline void outputOnShutdown(osvr::util::CSV &csv, std::ostream &out) {
    std::cout << "Writing out data..." << std::endl;
    csv.output(out);
}
inline void outputOnShutdown(osvr::util::StreamCSV & /*csv*/,
                             std::ostream & /*out*/) {
    // no-op for the streaming csv file.
}
template <typename CSVType>
void csvTypeSpecificMain(CSVType &csv,
                         std::vector<std::string> const &trackerPaths,
                         std::ostream &out) {
    using TrackerDataType = TrackerData<CSVType>;

    osvr::server::registerShutdownHandler<signalShutdown>();

    osvr::clientkit::ClientContext context("org.osvr.TrackerToCSV");

    /// Set up the interfaces
    bool needGroup = trackerPaths.size() > 1;
    std::vector<TrackerDataType> trackers;
    {
        std::size_t num = 0;
        for (auto &path : trackerPaths) {
            std::cout << "Will record tracker at path " << path << std::endl;
            if (needGroup) {
#if 0 
                trackers.emplace_back(csv, context, path, std::to_string(num) + ".");
#else
                trackers.emplace_back(csv, context, path, PathAsGroup);
#endif
            } else {
                trackers.emplace_back(csv, context, path);
            }
            num++;
        }
    }

    /// Set up the callbacks.
    for (auto &data : trackers) {
        data.iface.registerCallback(&TrackerDataType::myTrackerCallback, &data);
        if (g_settings.recordVelocity) {
            data.iface.registerCallback(&TrackerDataType::myVelocityCallback,
                                        &data);
        }
    }

    /// Each loop wait a millisecond to see if we've been signalled to shut
    /// down.
    bool outputStarted = false;
    while (shouldContinue()) {
        startOutputCheck(csv, g_reportedTracker, outputStarted);
        context.update();
    }

    std::cout << "Got shut down signal." << std::endl;
    outputOnShutdown(csv, out);
}

static const auto DEFAULT_OUTFILE = "trackerdata.csv";
static const auto VELOCITY_SWITCH = "--velocity";
static const auto SINGLE_TIME_SWITCH = "--single-time";
static const auto STREAMING_SWITCH = "--streaming";
int usage(const char *argv0) {
    std::cerr << "Usage: " << argv0
              << " <trackerpath> [...] [outfile] [options]\n\n";
    std::cerr << "Argument order does not matter.\n";
    std::cerr << "<trackerpath> must be a full semantic path to a tracker "
                 "sensor starting with a leading /\n";
    std::cerr << "If not otherwise specified, the output filename will be "
              << DEFAULT_OUTFILE << "\n\n";
    std::cerr << "Options:\n";
    std::cerr
        << "\t" << VELOCITY_SWITCH
        << " will add velocity recording in addition to pose recording.\n";
    std::cerr << "\t" << SINGLE_TIME_SWITCH
              << " will record time as a single decimal column "
                 "normalized to start at/near 0, instead of the epoch-based "
                 "two field timestamp.\n";
    std::cerr << "\t" << STREAMING_SWITCH
              << " will write data to the file periodically, rather than "
                 "waiting until a clean shutdown.\n"
              << std::endl;
    return 1;
}

#if 0
namespace osvr {
namespace util {
    namespace args {
        /// Takes a predicate to find arguments. Found arguments not immediately
        /// followed by another argument or the end of the argument list have
        /// the following argument passed to the action and are removed from the
        /// arg
        /// list.
        /// Count of valid action invocations is returned.
        template <typename F, typename G>
        inline std::size_t handle_value_arg(ArgList &c, F &&predicate,
                                            G &&action) {

            if (c.size() < 2) {
                // Need at least two args for this to work.
                return 0;
            }
            auto origEnd = end(c);
            std::vector<bool> isCandidate(false, origSize);
            // Apply predicate
            std::transform(begin(c), origEnd, begin(isCandidate),
                           [&](std::string const &arg) {
                               return std::forward<F>(predicate)(arg);
                           });

            ArgList remainingArgs;
            auto beforeEnd = end(c);
            --beforeEnd;

            std::size_t count = 0;
            /// Loop through all but the last element (since we can't start a
            /// value arg there)
            for (auto it = begin(c), e = end(c); it != beforeEnd && it != e;
                 ++it) {
                if (std::forward<F>(predicate)(*it)) {
                    /// found a flag, next thing is an arg.
                    ++it;
                    ++count;
                    std::forward<G>(action)(*it);
                } else {
                    /// not a flag - steal it for the remaining args list.
                    remainingArgs.emplace_back(std::move(*it));
                }
            }
            /// Remaining args are now the only args.
            c.swap(remainingArgs);
            return count;
        }
        /// Takes a list of  "sequences" (something with operator== with
        /// std::string) to
        /// compare (case-sensitive) with every arg.
        ///
        /// Wraps `handle_arg()` (so it "consumes" those args it finds)
        ///
        /// @return true if at least one copy of the arg was found.
        template <typename SequenceType>
        inline bool handle_has_any_switch_of(
            ArgList &c, std::initializer_list<SequenceType> argSwitches) {
            return 0 < handle_arg(c, [&argSwitches](std::string const &arg) {
                       for (const auto &sw : argSwitches) {
                           if (sw == arg) {
                               return true;
                           }
                       }
                       return false;
                   });
        }
    } // namespace args
} // namespace util
} // namespace osvr
#endif

int main(int argc, char *argv[]) {
    auto withUsageError = [&] { return usage(argv[0]); };
    if (argc < 2) {
        std::cerr << "Must pass at least a semantic path to a tracker, and "
                     "optionally a filename, as command line arguments!"
                  << std::endl;
        return withUsageError();
    }
    using namespace osvr::util::args;

#ifdef USE_INTERVAL
    nextReport = myClock::now();
#endif
    std::string outfn = DEFAULT_OUTFILE;

    auto args = makeArgList(argc, argv);
    if (handle_has_any_switch_of(args, {"/h", "/help", "/?", "-h", "--help"})) {
        // save the poor souls using slashed Windows argument styles to ask for
        // help.
        return withUsageError();
    }
    g_settings.recordVelocity = handle_has_switch(args, VELOCITY_SWITCH);
    if (g_settings.recordVelocity) {
        std::cout << "Enabling velocity recording." << std::endl;
    }

    g_settings.singleTimeField = handle_has_switch(args, SINGLE_TIME_SWITCH);
    if (g_settings.singleTimeField) {
        std::cout << "Will record timestamps as a single column" << std::endl;
    }

    bool streaming = handle_has_switch(args, STREAMING_SWITCH);
    if (streaming) {
        std::cout << "Will stream output to a file rather than waiting for a "
                     "clean shutdown"
                  << std::endl;
    }

    std::vector<std::string> trackerPaths;
    handle_arg(args, [&](std::string const &arg) {
        if (!arg.empty() && arg[0] == '/') {
            std::cout << "Assuming that the leading slash means that " << arg
                      << " is a semantic path of a tracker." << std::endl;
            trackerPaths.push_back(arg);
            return true;
        }
        return false;
    });

    if (trackerPaths.empty()) {
        std::cerr << "No tracker path specified!" << std::endl;
        return withUsageError();
    }

    if (args.size() > 1) {
        std::cerr << "Too many unhandled arguments left over: expected at most "
                     "one output filename!"
                  << std::endl;
        return withUsageError();
    }

    if (!args.empty()) {
        // exactly one argument, call it an output filename.
        std::cout << "Assuming " << args[0]
                  << " is intended as the output filename." << std::endl;
        outfn = args[0];
    }

    std::cout << "Will dump data to file " << outfn << std::endl;

    std::ofstream out(outfn);
    if (!out) {
        std::cout << "Couldn't open output file!" << std::endl;
        return -1;
    }

    if (streaming) {
        std::unique_ptr<osvr::util::StreamCSV> csv(
            new osvr::util::StreamCSV(out));
        std::cout << "Ctrl-C to exit and finish saving data." << std::endl;
        csvTypeSpecificMain(*csv, trackerPaths, out);
    } else {
        std::unique_ptr<osvr::util::CSV> csv(new osvr::util::CSV());
        std::cout << "Ctrl-C to exit and save data." << std::endl;
        csvTypeSpecificMain(*csv, trackerPaths, out);
    }

    return 0;
}
