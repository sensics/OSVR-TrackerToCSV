/** @file
    @brief Implementation

    @date 2016

    @author
    Sensics, Inc.
    <http://sensics.com/osvr>
*/

// Copyright 2014-2016 Sensics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Internal Includes
#include <osvr/ClientKit/Context.h>
#include <osvr/ClientKit/Interface.h>
#include <osvr/Server/RegisterShutdownHandler.h>
#include <osvr/Util/Finally.h>
#include <osvr/Util/MiniArgsHandling.h>
#include <osvr/Util/TimeValue.h>

// Library/third-party includes
// - none

// Standard includes
#include <atomic>
#include <chrono>
#include <iostream>
#include <memory>

using namespace osvr::util;

std::atomic_bool g_run{true};
void signalShutdown() { g_run.store(false); }
bool shouldContinue() { return g_run.load(); }

struct GlobalSettings {
    bool showTime = false;
    bool singleTimeField = false;

    OSVR_TimeValue processTimeStamp(OSVR_TimeValue const *tv) {
        if (!singleTimeField) {
            return *tv;
        }
        if (!gotFirstTime_) {
            firstTime_ = *tv;
            gotFirstTime_ = true;
        }
        // subtract the first time stamp away.
        auto ret = *tv;
        osvrTimeValueDifference(&ret, &firstTime_);
        return ret;
    }

  private:
    bool gotFirstTime_ = false;
    OSVR_TimeValue firstTime_ = {};

} g_settings;

static const struct PathAsGroup_t {
} PathAsGroup;

struct TrackerData {
    /// constructor for non-grouped (single tracker)
    TrackerData(osvr::clientkit::ClientContext &ctx, std::string const &p)
        : TrackerData(ctx, p, false, "") {}

    /// constructor for grouped, numbered trackers.
    TrackerData(osvr::clientkit::ClientContext &ctx, std::string const &p,
                std::size_t num)
        : TrackerData(ctx, p, true, std::to_string(num) + ".") {}

    /// constructor for trackers with group names from their paths
    TrackerData(osvr::clientkit::ClientContext &ctx, std::string const &p,
                PathAsGroup_t const &)
        : TrackerData(ctx, p, true, p + ":") {}

    std::string path;

    bool group = false;
    std::string groupHeader;
    std::string velGroupHeader;
    std::string angVelDtHeader;
    std::string angVelDeltaGroupHeader;

    osvr::clientkit::Interface iface;

    bool reported = false;

  private:
    /// delegated-to constructor.
    TrackerData(osvr::clientkit::ClientContext &ctx, std::string const &p,
                bool needGroup, std::string const &groupHdr)
        : path(p), group(needGroup), groupHeader(groupHdr),
          velGroupHeader(groupHeader + "vel."),
          angVelDtHeader(groupHeader + "angvel_dt"),
          angVelDeltaGroupHeader(groupHeader + "angvel_delta."),
          iface(ctx.getInterface(path)) {}
};

static const auto colWidth = 10;
static const auto fixedPrecision = 5;

template <typename T> inline void outputColumn(T const &val) {
    auto restoreState = osvr::util::finally(
        [&](std::ios::fmtflags origFlags = std::cout.flags()) {
            std::cout.flags(origFlags);
        });

    std::cout << std::setw(colWidth) << val;
}

inline void outputColumn(double val) {

    auto restoreState = osvr::util::finally(
        [&](std::ios::fmtflags origFlags = std::cout.flags()) {
            std::cout.flags(origFlags);
        });
    std::cout << std::setw(colWidth) << std::setprecision(fixedPrecision) << std::fixed
              << val;
}

void myTrackerCallback(void *userdata, const OSVR_TimeValue *timestamp,
                       const OSVR_PoseReport *report) {
    auto *data = static_cast<TrackerData *>(userdata);
    using namespace osvr::util;

    if (g_settings.showTime) {
        auto tv = g_settings.processTimeStamp(timestamp);
        if (g_settings.singleTimeField) {
            outputColumn(time::toDecimalString(tv));
        } else {
            outputColumn(tv.seconds);
            outputColumn(tv.microseconds);
        }
        std::cout << " :";
    }
#if 0
    auto outDouble = [](double val) {
        std::cout << std::setw(colWidth) << std::setprecision(4) << std::fixed
                  << val;
    };
#endif
    for (int i = 0; i < 3; ++i) {
        outputColumn(report->pose.translation.data[i]);
    }
    std::cout << " :";
    for (int i = 0; i < 4; ++i) {
        outputColumn(report->pose.rotation.data[i]);
    }
    std::cout << "\n" << std::flush;
}
#if 0
void myVelocityCallback(void *userdata, const OSVR_TimeValue *timestamp,
                        const OSVR_VelocityReport *report) {
    auto *data = static_cast<TrackerData *>(userdata);
    auto row = g_csv->row();
    using namespace osvr::util;

    auto tv = g_settings.processTimeStamp(timestamp);
    if (g_settings.singleTimeField) {
        row << cellGroup<DecimalTimeFieldTag>(tv);
    } else {
        row << cellGroup<AbbreviatedTimeMemberFieldsTag>(tv);
    }

    if (report->state.linearVelocityValid) {
        row << cellGroup(data->velGroupHeader, report->state.linearVelocity);
    }
    if (report->state.angularVelocityValid) {
        row << cell(data->angVelDtHeader, report->state.angularVelocity.dt);
        row << cellGroup(data->angVelDeltaGroupHeader,
                         report->state.angularVelocity.incrementalRotation);
    }
}

static const auto VELOCITY_SWITCH = "--velocity";
#endif
static const auto SINGLE_TIME_SWITCH = "--single-time";
static const auto TIME_SWITCH = "--time";
int usage(const char *argv0) {
    std::cerr << "Usage: " << argv0 << " <trackerpath> [...] [options]\n\n";
    std::cerr << "Argument order does not matter.\n";
    std::cerr << "<trackerpath> must be a full semantic path to a tracker "
                 "sensor starting with a leading /\n";
#if 0
    std::cerr << "If not otherwise specified, the output filename will be "
              << DEFAULT_OUTFILE << "\n\n";
#endif
    std::cerr << "Options:\n";
#if 0
    std::cerr
        << "\t" << VELOCITY_SWITCH
        << " will add velocity recording in addition to pose recording.\n";
#endif
    std::cerr << "\t" << TIME_SWITCH
              << " will enable outputting timestamps for each report.\n";
    std::cerr << "\t" << SINGLE_TIME_SWITCH
              << " will output time as a single decimal column "
                 "normalized to start at/near 0, instead of the epoch-based "
                 "two field timestamp.\n"
              << std::endl;
    return 1;
}

int main(int argc, char *argv[]) {
    auto withUsageError = [&] { return usage(argv[0]); };
    if (argc < 2) {
        std::cerr << "Must pass at least a semantic path to a tracker as a "
                     "command line argument!"
                  << std::endl;
        return withUsageError();
    }
    using namespace osvr::util::args;

    auto args = makeArgList(argc, argv);
    if (handle_has_any_switch_of(args, {"/h", "/help", "/?", "-h", "--help"})) {
        // save the poor souls using slashed Windows argument styles to ask for
        // help.
        return withUsageError();
    }

    g_settings.showTime = handle_has_switch(args, TIME_SWITCH);
    if (g_settings.showTime) {
        std::cout << "Will output timestamps." << std::endl;
    }
    g_settings.singleTimeField = handle_has_switch(args, SINGLE_TIME_SWITCH);
    if (g_settings.showTime && g_settings.singleTimeField) {
        std::cout << "Will output timestamps as a single column" << std::endl;
    }

    std::vector<std::string> trackerPaths;
    handle_arg(args, [&](std::string const &arg) {
        if (!arg.empty() && arg[0] == '/') {
            std::cout << "Assuming that the leading slash means that " << arg
                      << " is a semantic path of a tracker." << std::endl;
            trackerPaths.push_back(arg);
            return true;
        }
        return false;
    });

    if (trackerPaths.empty()) {
        std::cerr << "No tracker path specified!" << std::endl;
        return withUsageError();
    }
    if (trackerPaths.size() > 1) {
        std::cerr << "Too many tracker paths specified - max of one supported "
                     "right now!"
                  << std::endl;
        return withUsageError();
    }

    if (!args.empty()) {
        std::cerr << "Unrecognized arguments!" << std::endl;
        return withUsageError();
    }

    auto trackerPath = trackerPaths.front();

    osvr::server::registerShutdownHandler<signalShutdown>();

    osvr::clientkit::ClientContext context("org.osvr.PrintTracker");

    TrackerData tracker(context, trackerPath);
    /// Set up the callbacks.
    tracker.iface.registerCallback(&myTrackerCallback, &tracker);

    /// Each loop wait a millisecond to see if we've been signalled to shut
    /// down.
    while (shouldContinue()) {

        context.update();
    }

    std::cout << "Got shut down signal." << std::endl;

    return 0;
}
